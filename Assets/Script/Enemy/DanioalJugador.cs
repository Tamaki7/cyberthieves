using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DanioalJugador : MonoBehaviour   
{
    public int vida = 100;
    public bool Mugen = false;
    public float tiempo_Invulnerable = 1f;
    public void RestarVida(int cantidad)
    {
        if (!Mugen && vida > 0)
        {
            vida -= cantidad;
            StartCoroutine(Invulnerable());
            if (vida == 0)
            {
                Gameover();
            }
        }
        void Gameover()
        {
            Time.timeScale = 0;
        }
    }
    IEnumerator Invulnerable()
    {
        Mugen = true;
        yield return new WaitForSeconds(tiempo_Invulnerable);
        Mugen = false;
    }
}

