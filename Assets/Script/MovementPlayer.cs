using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementPlayer : MonoBehaviour
{
    private Rigidbody rb;

    [Header("Movimiento")]
    public float velocidadDesplazamiento = 10f;
    public float distanciaPiso = 0.7f;

    [Header("Salto")]
    public float jumpForce = 5f;
    public float jumpCD = 0.05f;
    public bool puedeSalto;

    [Header("Teclas de Acceso")]
    public KeyCode Saltar = KeyCode.Space;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        puedeSalto = true;
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
        MisInputs();

        // Obtener la entrada del jugador
        float horizontalInput = Input.GetAxis("Horizontal");
        float verticalInput = Input.GetAxis("Vertical");

        // Calcular la direcci�n de movimiento en relaci�n con la c�mara
        Vector3 cameraForward = Camera.main.transform.forward;
        Vector3 cameraRight = Camera.main.transform.right;
        cameraForward.y = 0f; // Asegurarse de que la direcci�n no tenga componente vertical
        cameraRight.y = 0f;
        cameraForward.Normalize();
        cameraRight.Normalize();

        // Calcular la direcci�n de movimiento
        Vector3 movement = (cameraForward * verticalInput + cameraRight * horizontalInput).normalized;

        // Mover al jugador en la direcci�n calculada
        transform.Translate(movement * velocidadDesplazamiento * Time.deltaTime);
    }

    void MisInputs()
    {
        
            if (Input.GetKey(Saltar))
        {
            if (EstaEnPiso() && puedeSalto)
            {

                puedeSalto = false;

                Jump();

                Invoke(nameof(ResetSalto), jumpCD);
            }

        }
    }

    void Jump()
    {
        rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
    }

    private void ResetSalto()
    {
        puedeSalto = true;
    }

    void Movement()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * velocidadDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * velocidadDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);
    }

    private bool EstaEnPiso()
    {
        return Physics.Raycast(transform.position, Vector3.down, distanciaPiso);
    }

    

}
